﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mvc2019.Models
{
    public class ClientInfo
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string HiddenData { get; set; }
        public Boolean Adult { get; set; }
        public string Sex { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}