﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mvc2019.Models;

namespace mvc2019.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult TableHtml()
        {


            return View();
        }

        public ActionResult TableHtmlKnock()
        {
            return View();
        }

        [HttpPost]
        public ActionResult TableInfo(List<ClientInfo> Clients)
        {


            return View();
        }

        [HttpPost]
        public ActionResult TableInfoKnockOut(List<ClientInfo> Clients)
        {


            return View();
        }

    }
}